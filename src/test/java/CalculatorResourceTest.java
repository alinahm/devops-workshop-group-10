import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = "300+98+1";
        assertEquals("399", calculatorResource.calculate(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals("899", calculatorResource.calculate(expression));

        expression = "20-2-2";
        assertEquals("16", calculatorResource.calculate(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "12/2/2";
        assertEquals("3", calculatorResource.calculate(expression));
    }

    @Test
    public void testMultiplication() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "3*2*2";
        assertEquals("12", calculatorResource.calculate(expression));
    }

    @Test
    public void testError() {
        CalculatorResource calculatorResource = new CalculatorResource();
        String expression = "3*2-1+8";
        assertEquals("Error", calculatorResource.calculate(expression));
    }
}

package resources;

import com.google.protobuf.TextFormatParseLocation;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {

    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String calculate(String expression){
        // Removes all whitespaces
        String expressionTrimmed = expression.replaceAll("\\s+","");

        int result = 0;

        /*
         * .matches use regex expression to decide if a String matches a given pattern.
         * [0-9]+[+][0-9]+ explained:
         * [0-9]+: a number from 0-9 one or more times. For example 1, 12 and 123.
         * [+]: the operator + one time.
         * The total regex expression accepts for example:
         * 12+34,
         * 1+2,
         * 10000+1000
         */
        if(expressionTrimmed.matches("^\\d+(?:\\s*[+]\\s*\\d+)*$")) result = sum(expressionTrimmed);
        else if(expressionTrimmed.matches("^\\d+(?:\\s*[-]\\s*\\d+)*$")) result = subtraction(expressionTrimmed);
        else if(expressionTrimmed.matches("^\\d+(?:\\s*[*]\\s*\\d+)*$")) result = multiplication(expressionTrimmed);
        else if(expressionTrimmed.matches("^\\d+(?:\\s*[/]\\s*\\d+)*$")) result = division(expressionTrimmed);
        else return "Error";

        return String.valueOf(result);
    }

    /**
     * Method used to calculate a sum expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int sum(String expression){
        String[] split = expression.split("[+]");

        int total = 0;

        for (String s : split) total += Integer.parseInt(s);

        return total;
    }

    /**
     * Method used to calculate a subtraction expression.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public int subtraction(String expression){
        String[] split = expression.split("[-]");

        int total = 0;

        for (int i = 0; i < split.length - 1; i++) {
            if (i == 0) {
                int number1 = Integer.parseInt(split[i]);
                int number2 = Integer.parseInt(split[i+1]);
                total = number1 - number2;
            } else {
                int number2 = Integer.parseInt(split[i+1]);
                total = total - number2;
            }
        }

        return total;
    }

    /**
     * This method multiplies two integers.
     *
     * @param equation  The equation to be multiplied.
     * @return          The multiplied number.
     */
    private int multiplication(String equation) {
        String[] split = equation.split("[*]");

        int total = 0;

        for (int i = 0; i < split.length - 1; i++){
            if (i == 0) {
                int number1 = Integer.parseInt(split[i]);
                int number2 = Integer.parseInt(split[i+1]);
                total = number1 * number2;
            } else {
                int number2 = Integer.parseInt(split[i+1]);
                total = total * number2;
            }
        }
        return total;
    }

    /**
     * This method divides two number.
     *
     * @param equation  The string of the equation.
     * @return          The divided number.
     */
    private int division(String equation) {
        String[] split = equation.split("[/]");

        int total = 0;

        for (int i = 0; i < split.length - 1; i++){
            if (i == 0) {
                int number1 = Integer.parseInt(split[i]);
                int number2 = Integer.parseInt(split[i+1]);
                total = number1 / number2;
            } else {
                int number2 = Integer.parseInt(split[i+1]);
                total = total / number2;
            }
        }
        //int number1 = Integer.parseInt(split[0]);
        //int number2 = Integer.parseInt(split[1]);

        return total;
    }
}
